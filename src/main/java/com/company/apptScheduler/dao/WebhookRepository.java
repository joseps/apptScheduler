package com.company.apptScheduler.dao;

import com.company.apptScheduler.entity.Webhook;
import org.springframework.data.repository.CrudRepository;

/**
 * Webhook Repository
 */
public interface WebhookRepository  extends CrudRepository<Webhook, Long> {
}
