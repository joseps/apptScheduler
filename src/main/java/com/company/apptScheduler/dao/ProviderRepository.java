package com.company.apptScheduler.dao;

import com.company.apptScheduler.entity.Provider;
import org.springframework.data.repository.CrudRepository;

/**
 * Provider Repository
 */
public interface ProviderRepository extends CrudRepository<Provider, Long> {
}
