package com.company.apptScheduler.dao;

import com.company.apptScheduler.entity.Department;
import org.springframework.data.repository.CrudRepository;

/**
 * Department Repository
 */
public interface DepartmentRepository extends CrudRepository<Department, Long> {
}

