package com.company.apptScheduler.dao;

import com.company.apptScheduler.entity.Patient;
import org.springframework.data.repository.CrudRepository;

/**
 * Patient Repository
 */
public interface PatientRepository extends CrudRepository<Patient, Long> {
}

