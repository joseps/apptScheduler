package com.company.apptScheduler.dto;

/**
 * Used to notify appointment change events
 */
public enum Event {
    CREATED,
    UPDATED;
}
